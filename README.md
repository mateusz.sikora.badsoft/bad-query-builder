# Bad Query Builder (BQB)

This library contains a utils for build native SQL queries.
Generated query can be executed in `JdbcTemplate`.

## Maven

Add the following dependency to your POM:

```xml

<dependency>
    <groupId>pl.badsoft</groupId>
    <artifactId>bad-query-builder</artifactId>
    <version>1.0.0</version>
</dependency>
```

You need add a gitlab Package Registry to your POM:

```xml

<repositories>
    <repository>
        <id>gitlab-maven</id>
        <url>https://gitlab.com/api/v4/projects/57515761/packages/maven</url>
    </repository>
</repositories>
```

## Gradle

Add the following dependency to your build.gradle:

```gradle
compile 'pl.badsoft:bad-query-builder:1.0.10'
```

## SQL queries suppoerted:

- select
- insert
- update
- delete

## How to use

### Builders

Builders simplify the creation of SQL strings.
```java
// Select with where clause
QuerySelect querySelect = QuerySelect.Builder.of()
		.select(new Select("id","name","last_name"))
		.from(new From("person"))
		.where(new Where(Condition.of("name", EQUAL, "Mateusz")))
		.build();
querySelect.toString();
```

The result of this code `SELECT id,name,last_name FROM person WHERE name ='Mateusz`
***

```java
// Select with join clause
QuerySelect querySelect = QuerySelect.builder()
		.select(new Select())
		.from(new From("user", "u"))
		.join(new Join(JoinType.LEFT_JOIN, "person p", "u.person_id", "p.id"))
		.build();
querySelect.toString();
```

The result of this code `SELECT * FROM user u LEFT JOIN person p ON u.person_id=p.id`
***

```java
// Insert statement
QueryInsert queryInsert = QueryInsert.builder()
		.table("person")
		.values(List.of(new Value("id", 2), new Value("name", "Adam"), new Value("login_date_time", "2021-08-13T13:07:00"))
				.build();
queryInsert.toString();
```

The result of this code `INSERT INTO person (id,name,login_date_time) VALUES (2,'Adam','2021-08-13T13:07:00')`
***

### Builders by raw SQL

```java
QuerySelect querySelect = QuerySelect.of
		.select(Select.ofString("id,name,last_login"))
		.from(From.ofString("person p"))
		.where(Where.ofString("p.name = 'Mateusz' AND p.last_login >='2024-01-01'"))
		.build();
querySelect.toString();
```

The result of this
code `SELECT id,name,last_login FROM person p WHERE p.name ='Mateusz' AND p.last_login >='2024-01-01'`

