package pl.badsoft.bqb.condition;

import lombok.AllArgsConstructor;
import lombok.Getter;
import pl.badsoft.bqb.util.ValueFormater;

import static org.apache.commons.lang3.StringUtils.SPACE;

@Getter
@AllArgsConstructor
public class Condition {

	private String column;
	private ConditionOperator operator;
	private Object value;

	private ConditionOperator relationOperator;
	private Condition nextCondition;


	public Condition(String column, ConditionOperator operator, Object value) {
		this.column = column;
		this.operator = operator;
		this.value = value;
	}

	public static Condition of(String column, ConditionOperator operator) {
		return new Condition(column, operator, null);
	}

	public static Condition of(String column, ConditionOperator operator, Object value) {
		return new Condition(column, operator, value);
	}

	public Condition and(Condition nextCondition) {
		this.relationOperator = ConditionOperator.AND;
		this.nextCondition = nextCondition;
		return this;
	}

	public Condition or(Condition nextCondition) {
		this.relationOperator = ConditionOperator.OR;
		this.nextCondition = nextCondition;
		return this;
	}

	@Override
	public String toString() {
		ConditionValidator.validateCondition(this);
		if (nextCondition != null && relationOperator != null) {
			return column + SPACE + operator.toString() + ValueFormater.formattingValue(value) + SPACE + relationOperator.toString() + SPACE + nextCondition.toString();
		}
		return column + SPACE + operator.toString() + ValueFormater.formattingValue(value);
	}
}
