package pl.badsoft.bqb.condition;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum ConditionOperator {

	EQUAL("="), NOT_EQUAL("!="),
	IN("IN"), NOT_IN("NOT IN"),
	LIKE("LIKE"),
	// TODO:	BETWEEN("BETWEEN"),
	IS_NULL("IS NULL"), IS_NOT_NULL("IS NOT NULL"),
	GREATER(">"), GREATER_OR_EQUALS(">="),
	LESS("<"), LESS_OR_EQUALS("<="),
	AND("AND"), OR("OR");

	private final String value;

	@Override
	public String toString() {
		return value;
	}
}
