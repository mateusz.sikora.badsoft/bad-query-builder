package pl.badsoft.bqb.condition;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import pl.badsoft.bqb.exception.ConditionValidatorException;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
class ConditionValidator {

	static void validateCondition(Condition condition) {
		boolean allNull = (condition.getNextCondition() == null && condition.getRelationOperator() == null);
		boolean allSet = (condition.getNextCondition() != null && condition.getRelationOperator() != null);
		if (!allNull && !allSet) {
			throw new ConditionValidatorException("Next condition or related operator can not be null if one of that elements is set");
		}
	}
}
