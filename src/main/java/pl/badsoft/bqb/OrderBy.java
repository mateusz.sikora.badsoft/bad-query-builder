package pl.badsoft.bqb;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import pl.badsoft.bqb.util.Sort;

public class OrderBy implements Section {

	private static final String ORDER_BY = "ORDER BY";

	private final List<Sort> columns;

	public OrderBy() {
		this.columns = List.of(new Sort("id"));
	}

	public OrderBy(Sort... columns) {
		this.columns = Arrays.stream(columns).distinct().toList();
	}

	public static OrderBy of(Sort sort) {
		return new OrderBy(sort);
	}

	public static OrderBy of(String column, String direction) {
		Sort sort = Sort.of(column, Sort.Direction.valueOf(direction));
		return new OrderBy(sort);
	}

	public static OrderBy of(String column, Sort.Direction direction) {
		Sort sort = Sort.of(column, direction);
		return new OrderBy(sort);
	}

	@Override
	public String toString() {
		if (columns == null || columns.isEmpty()) {
			throw new IllegalArgumentException("Column can not be null or empty");
		}
		return ORDER_BY + " " + StringUtils.join(columns, ",");
	}
}
