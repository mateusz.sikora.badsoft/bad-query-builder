package pl.badsoft.bqb.exception;

public class RequiredValueException extends RuntimeException {

	public RequiredValueException() {
	}

	public RequiredValueException(String message) {
		super(message);
	}
}
