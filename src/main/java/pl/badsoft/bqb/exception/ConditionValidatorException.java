package pl.badsoft.bqb.exception;

public class ConditionValidatorException extends RuntimeException {

	public ConditionValidatorException() {
	}

	public ConditionValidatorException(String message) {
		super(message);
	}
}
