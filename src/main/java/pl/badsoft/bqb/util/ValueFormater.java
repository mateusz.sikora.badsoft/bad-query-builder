package pl.badsoft.bqb.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Objects;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import static org.apache.commons.lang3.StringUtils.EMPTY;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ValueFormater {

	private static final String STRING_FORMAT = "'%s'";

	public static String formattingValue(Object value) {
		return switch (value) {
			case null -> EMPTY;
			case String str -> String.format(STRING_FORMAT, str);
			case Number num -> num.toString();
			case Boolean bool -> bool.toString();
			case LocalDate ld -> String.format(STRING_FORMAT, ld.format(DateTimeFormatter.ISO_DATE));
			case LocalDateTime ldt -> String.format(STRING_FORMAT, ldt.format(DateTimeFormatter.ISO_DATE_TIME));
			case Collection<?> values -> formattingCollection(values);
			default -> throw new IllegalArgumentException("Type not supported");
		};
	}

	private static String formattingCollection(Collection<?> values) {
		String formatedCollection = values.stream().map(o -> {
					if (o instanceof String) {
						return "'" + o + "'";
					} else if (o instanceof Number num) {
						return num.toString();
					}
					return null;
				})
				.filter(Objects::nonNull)
				.reduce((s, s2) -> s + "," + s2)
				.orElseThrow();
		return " (" + formatedCollection + ")";
	}
}
