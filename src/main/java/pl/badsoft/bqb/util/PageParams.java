package pl.badsoft.bqb.util;

import lombok.Getter;
import lombok.Setter;
import pl.badsoft.bqb.OrderBy;

@Getter
public class PageParams {

	private Integer pageSize;
	private Integer offset;
	@Setter
	private OrderBy orderBy;

	public PageParams(Integer pageSize, Integer offset) {
		this.pageSize = pageSize;
		this.offset = offset;
	}

	public PageParams(Integer pageSize, Integer offset, OrderBy orderBy) {
		this.pageSize = pageSize;
		this.offset = offset;
		this.orderBy = orderBy;
	}

	public static PageParams of(Integer pageSize, Integer offset) {
		return new PageParams(pageSize, offset);
	}

	public static PageParams of(Integer pageSize, Integer offset, OrderBy orderBy) {
		return new PageParams(pageSize, offset, orderBy);
	}

	public static PageParams of(Integer pageSize, Integer offset, String property, Sort.Direction direction) {
		return new PageParams(pageSize, offset, new OrderBy(Sort.of(property, direction)));
	}

	public static PageParams of(Integer pageSize, Integer offset, String property) {
		return new PageParams(pageSize, offset, new OrderBy(Sort.of(property)));
	}

	public boolean isSorted() {
		return orderBy != null;
	}
}
