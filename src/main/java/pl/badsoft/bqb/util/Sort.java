package pl.badsoft.bqb.util;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;

@Getter
@EqualsAndHashCode
public class Sort {

	private final String property;
	private final Direction direction;

	public Sort(@NonNull String property) {
		this.property = property;
		this.direction = Direction.ASC;
	}

	public Sort(@NonNull String property, @NonNull Direction direction) {
		this.property = property;
		this.direction = direction;
	}

	public static Sort of(String property) {
		return new Sort(property, Direction.ASC);
	}

	public static Sort of(String property, Direction direction) {
		return new Sort(property, direction);
	}

	@Override
	public String toString() {
		return property + " " + direction;
	}

	public enum Direction {
		ASC, DESC;
	}
}
