package pl.badsoft.bqb.util;

import java.util.ArrayList;
import java.util.List;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import pl.badsoft.bqb.OrderBy;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PageUtils {

	public static String buildPageClause(PageParams pageable) {
		return buildPageClause(pageable.getPageSize(), pageable.getOffset(), pageable.getOrderBy());
	}

	public static String buildPageClause(Integer pageSize, Integer offset, OrderBy orderBy) {
		List<String> section = new ArrayList<>(3);

		if (orderBy != null) {
			section.add(orderBy.toString());
		}
		if (pageSize != null) {
			section.add("LIMIT " + pageSize);
		}
		if (offset != null) {
			section.add("OFFSET " + offset);
		}
		return StringUtils.join(section, " ");
	}
}
