package pl.badsoft.bqb.join;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum JoinType {
	JOIN("JOIN"),
	INNER_JOIN("INNER JOIN"),
	LEFT_JOIN("LEFT JOIN"),
	RIGHT_JOIN("RIGHT JOIN"),
	FULL_JOIN("FULL JOIN");

	private final String value;

	@Override
	public String toString() {
		return value;
	}
}
