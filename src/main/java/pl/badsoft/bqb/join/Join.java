package pl.badsoft.bqb.join;

import org.apache.commons.lang3.StringUtils;
import pl.badsoft.bqb.Section;
import pl.badsoft.bqb.exception.RequiredValueException;

public class Join implements Section {

	private final JoinType joinType;
	private final String table;
	private final String columnLeft;
	private final String columnRight;

	public Join(JoinType joinType, String table, String columnLeft, String columnRight) {
		this.joinType = joinType;
		this.table = table;
		this.columnLeft = columnLeft;
		this.columnRight = columnRight;
	}

	@Override
	public String toString() {
		if (joinType == null || StringUtils.isAnyBlank(table, columnLeft, columnRight)) {
			throw new RequiredValueException("JoinType, table name or columns can not be null or empty");
		}
		return joinType.getValue() + " " + table + " ON " + columnLeft + "=" + columnRight;
	}
}
