package pl.badsoft.bqb;

import org.apache.commons.lang3.StringUtils;

public class From extends RawSection implements Section {

	private static final String FROM = "FROM";

	private final String table;
	private final String alias;

	public From() {
		this.table = null;
		this.alias = null;
	}

	public From(String table) {
		this.table = table;
		this.alias = null;
	}

	public From(String table, String alias) {
		this.table = table;
		this.alias = alias;
	}

	public static From of(String table) {
		return new From(table);
	}

	public static From of(String table, String alias) {
		return new From(table, alias);
	}

	public static From ofString(String rawStatement) {
		From from = new From();
		from.setRawStatement(rawStatement);
		return from;
	}

	@Override
	public String toString() {
		if (isRawStatement()) {
			return FROM + " " + getRawStatement();
		}
		if (StringUtils.isBlank(table)) {
			throw new IllegalArgumentException("Table name can not be null or empty");
		}
		String fromClause = FROM + " " + table;
		if (StringUtils.isNotBlank(alias)) {
			return fromClause + " " + alias;
		}
		return fromClause;
	}
}
