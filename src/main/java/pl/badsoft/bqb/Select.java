package pl.badsoft.bqb;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

public class Select extends RawSection implements Section {

	private static final String SELECT = "SELECT";

	private final List<String> columns;

	public Select() {
		this.columns = List.of("*");
	}

	public Select(String... columns) {
		this.columns = Arrays.stream(columns).distinct().toList();
	}

	public static Select of(String... columns) {
		return new Select(columns);
	}

	public static Select ofString(String rawStatement) {
		Select select = new Select();
		select.setRawStatement(rawStatement);
		return select;
	}

	@Override
	public String toString() {
		if (isRawStatement()) {
			return SELECT + " " + getRawStatement();
		}
		if (columns == null || columns.isEmpty()) {
			throw new IllegalArgumentException("Column can not be null or empty");
		}
		return SELECT + " " + StringUtils.join(columns, ",");
	}
}
