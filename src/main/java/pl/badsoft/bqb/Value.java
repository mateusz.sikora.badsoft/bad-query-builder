package pl.badsoft.bqb;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import pl.badsoft.bqb.util.ValueFormater;

@Getter
@Setter
@AllArgsConstructor
public class Value {

	private String column;
	private Object value;

	public String getFormattedValue() {
		if (value == null) {
			return "null";
		}
		return ValueFormater.formattingValue(value);
	}

	@Override
	public String toString() {
		return column + "=" + getFormattedValue();
	}
}
