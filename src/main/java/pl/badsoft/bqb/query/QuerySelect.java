package pl.badsoft.bqb.query;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import pl.badsoft.bqb.From;
import pl.badsoft.bqb.OrderBy;
import pl.badsoft.bqb.Section;
import pl.badsoft.bqb.Select;
import pl.badsoft.bqb.Where;
import pl.badsoft.bqb.join.Join;
import pl.badsoft.bqb.util.PageParams;
import pl.badsoft.bqb.util.PageUtils;

import static org.apache.commons.lang3.StringUtils.SPACE;

@AllArgsConstructor
@Slf4j
public class QuerySelect {

	private List<Section> sections;
	private PageParams pageParams;

	public static QuerySelect.Builder builder() {
		return new QuerySelect.Builder();
	}

	public QuerySelect(List<Section> sections) {
		this.sections = sections;
	}

	@Override
	public String toString() {
		String query = sections.stream()
				.filter(Objects::nonNull)
				.map(Object::toString)
				.collect(Collectors.joining(" "));
		log.debug("QuerySelect query: {}", query);
		if (pageParams != null) {
			return query + SPACE + PageUtils.buildPageClause(pageParams);
		}
		return query;
	}

	public static class Builder {
		private Select select;
		private From from;
		private Where where;
		private Join join;
		private OrderBy orderBy;
		private PageParams pageParams;

		public Builder select() {
			this.select = new Select();
			return this;
		}

		public Builder select(Select select) {
			this.select = select;
			return this;
		}

		public Builder from(From from) {
			this.from = from;
			return this;
		}

		public Builder from(String from) {
			this.from = new From(from);
			return this;
		}

		public Builder where(Where where) {
			this.where = where;
			return this;
		}

		public Builder join(Join join) {
			this.join = join;
			return this;
		}

		public Builder orderBy(OrderBy orderBy) {
			this.orderBy = orderBy;
			return this;
		}

		public Builder pageParams(PageParams pageParams) {
			this.pageParams = pageParams;
			return this;
		}

		public QuerySelect build() {
			if (orderBy != null && pageParams != null && pageParams.isSorted()) {
				pageParams.setOrderBy(orderBy);
				orderBy = null;
			}
			if (pageParams != null) {
				return new QuerySelect(Arrays.asList(select, from, join, where, orderBy), pageParams);
			}
			return new QuerySelect(Arrays.asList(select, from, join, where, orderBy));
		}
	}
}
