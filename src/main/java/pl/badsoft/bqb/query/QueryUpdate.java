package pl.badsoft.bqb.query;

import java.util.List;
import java.util.stream.Collectors;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import pl.badsoft.bqb.Value;
import pl.badsoft.bqb.Where;
import pl.badsoft.bqb.exception.RequiredValueException;

@Slf4j
@AllArgsConstructor
public class QueryUpdate {

	private static final String UPDATE = "UPDATE";
	private static final String SET = "SET";
	private final String table;
	private List<Value> values;
	private Where where;

	public static QueryUpdate.Builder builder() {
		return new QueryUpdate.Builder();
	}

	@Override
	public String toString() {
		String valueSection = values.stream()
				.map(Value::toString)
				.collect(Collectors.joining(", "));

		String query = UPDATE + " " + table + " " + SET + " " + valueSection;
		if (where != null) {
			query += " " + where;
		}
		log.debug("QueryUpdate query: {}", query);
		return query;
	}

	public static class Builder {
		private String table;
		private Where where;
		private List<Value> values;

		public QueryUpdate.Builder table(String table) {
			this.table = table;
			return this;
		}

		public QueryUpdate.Builder values(List<Value> values) {
			this.values = values;
			return this;
		}

		public QueryUpdate.Builder where(Where where) {
			this.where = where;
			return this;
		}

		public QueryUpdate build() {
			if (StringUtils.isBlank(table) || CollectionUtils.isEmpty(values)) {
				throw new RequiredValueException("Table name or values can not be null");
			}
			return new QueryUpdate(table, values, where);
		}
	}

}
