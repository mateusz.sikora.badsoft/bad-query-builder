package pl.badsoft.bqb.query;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import pl.badsoft.bqb.Value;
import pl.badsoft.bqb.exception.RequiredValueException;

@AllArgsConstructor
@Slf4j
public class QueryInsert {

	private static final String INSERT_INTO = "INSERT INTO";
	private static final String VALUES = "VALUES";
	private final String table;
	private final List<Value> values;

	public static QueryInsert.Builder builder() {
		return new QueryInsert.Builder();
	}

	@Override
	public String toString() {
		List<String> columns = new ArrayList<>(values.size());
		List<String> data = new ArrayList<>(values.size());
		for (Value v : values) {
			columns.add(v.getColumn());
			data.add(v.getFormattedValue());
		}
		String query = INSERT_INTO + " " + table + " (" + StringUtils.join(columns, ",") + ") "
				+ VALUES + " (" + StringUtils.join(data, ",") + ")";
		log.debug("QueryInsert query: {}", query);
		return query;
	}

	public static class Builder {
		private String table;
		private List<Value> values;

		public Builder table(String table) {
			this.table = table;
			return this;
		}

		public Builder values(List<Value> values) {
			this.values = values;
			return this;
		}

		public QueryInsert build() {
			if (StringUtils.isBlank(table) || CollectionUtils.isEmpty(values)) {
				throw new RequiredValueException("Table name or values can not be null");
			}
			return new QueryInsert(table, values);
		}
	}
}
