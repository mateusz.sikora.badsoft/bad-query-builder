package pl.badsoft.bqb.query;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import pl.badsoft.bqb.Where;
import pl.badsoft.bqb.exception.RequiredValueException;

@AllArgsConstructor
@Slf4j
public class QueryDelete {

	private static final String DELETE = "DELETE FROM";
	private String table;
	private Where where;

	public static QueryDelete.Builder builder() {
		return new QueryDelete.Builder();
	}

	@Override
	public String toString() {
		String query = DELETE + " " + table;
		if (where != null) {
			query += " " + where.toString();
		}
		log.debug("QueryDelete query: {}", query);
		return query;
	}

	public static class Builder {
		private String table;
		private Where where;

		public Builder table(String table) {
			this.table = table;
			return this;
		}

		public Builder where(Where where) {
			this.where = where;
			return this;
		}

		public QueryDelete build() {
			if (StringUtils.isBlank(table)) {
				throw new RequiredValueException("Table name can not be null");
			}
			return new QueryDelete(table, where);
		}
	}
}
