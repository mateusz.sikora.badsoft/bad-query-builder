package pl.badsoft.bqb;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

@Getter
@Setter
abstract class RawSection {

	private String rawStatement;

	protected RawSection() {
	}

	protected RawSection(String rawStatement) {
		this.rawStatement = rawStatement;
	}

	boolean isRawStatement() {
		return StringUtils.isNotBlank(rawStatement);
	}

}
