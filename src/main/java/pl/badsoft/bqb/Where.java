package pl.badsoft.bqb;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import pl.badsoft.bqb.condition.Condition;
import pl.badsoft.bqb.condition.ConditionOperator;

import static pl.badsoft.bqb.condition.ConditionOperator.AND;

public class Where extends RawSection implements Section {

	private static final String WHERE = "WHERE";

	private final List<Condition> conditions;
	private final ConditionOperator conditionOperator;

	public Where(Condition... conditions) {
		this.conditions = Arrays.stream(conditions).toList();
		conditionOperator = AND;
	}

	public Where(ConditionOperator conditionOperator, Condition... conditions) {
		this.conditions = Arrays.stream(conditions).toList();
		this.conditionOperator = conditionOperator;
	}

	public static Where of(Condition... conditions) {
		return new Where(conditions);
	}

	public static Where of(ConditionOperator conditionOperator, Condition... conditions) {
		return new Where(conditionOperator, conditions);
	}

	public static Where ofString(String rawStatement) {
		Where where = new Where();
		where.setRawStatement(rawStatement);
		return where;
	}

	@Override
	public String toString() {
		if (isRawStatement()) {
			return WHERE + " " + getRawStatement();
		}
		if (conditions == null || conditions.isEmpty()) {
			throw new IllegalArgumentException("Where can not be null or empty");
		}

		if (conditions.size() == 1) {
			return WHERE + " " + conditions.get(0).toString();
		}

		List<String> strCoditions = conditions.stream()
				.map(condition -> "(" + condition.toString() + ")")
				.toList();
		String separator = " " + conditionOperator + " ";
		return WHERE + " " + StringUtils.join(strCoditions, separator);
	}
}
