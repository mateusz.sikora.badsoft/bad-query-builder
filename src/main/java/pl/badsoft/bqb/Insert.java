package pl.badsoft.bqb;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import pl.badsoft.bqb.exception.RequiredValueException;

public class Insert implements Section {

	private static final String INSERT_INTO = "INSERT INTO";

	private final String table;
	private final List<String> columns;

	public Insert(String table, String... columns) {
		this.table = table;
		this.columns = Arrays.stream(columns).distinct().toList();
	}

	public static Insert of(String table, String... columns) {
		return new Insert(table, columns);
	}

	@Override
	public String toString() {
		if (columns == null || columns.isEmpty() || StringUtils.isBlank(table)) {
			throw new RequiredValueException("Column or table name can not be null or empty");
		}
		return INSERT_INTO + " " + table + " (" + StringUtils.join(columns, ",") + ")";
	}
}
