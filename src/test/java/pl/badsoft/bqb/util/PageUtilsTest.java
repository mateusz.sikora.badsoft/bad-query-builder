package pl.badsoft.bqb.util;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class PageUtilsTest {

	@Test(dataProvider = "valueDataProvider")
	public void buildPageClause_Success(PageParams pageable, String result) {
		Assert.assertEquals(PageUtils.buildPageClause(pageable), result);
	}

	@DataProvider(name = "valueDataProvider")
	Object[][] valueDataProvider() {
		return new Object[][]{
				{PageParams.of(500, 0), "LIMIT 500 OFFSET 0"},
				{PageParams.of(500, 0, "email", Sort.Direction.ASC), "ORDER BY email ASC LIMIT 500 OFFSET 0"},
				{PageParams.of(500, 0, "email"), "ORDER BY email ASC LIMIT 500 OFFSET 0"},
				{PageParams.of(500, 0, "email", Sort.Direction.DESC), "ORDER BY email DESC LIMIT 500 OFFSET 0"},
				{PageParams.of(500, null), "LIMIT 500"},
				{PageParams.of(null, 0), "OFFSET 0"},
		};
	}
}