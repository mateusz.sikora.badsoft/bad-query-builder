package pl.badsoft.bqb;

import java.util.Arrays;
import java.util.StringJoiner;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class SelectTest {

	@Test(dataProvider = "valueDataProvider")
	void select_Statement_Success(SelectData selectData) {
		Select select = new Select(selectData.columns);
		Assert.assertEquals(select.toString(), selectData.result);
	}

	@DataProvider(name = "valueDataProvider")
	SelectData[] valueDataProvider() {
		return new SelectData[]{
				new SelectData(new String[]{"id", "name", "phone"}, "SELECT id,name,phone"),
				new SelectData(new String[]{"*"}, "SELECT *"),
				new SelectData(new String[]{"name"}, "SELECT name")
		};
	}

	@Test()
	void select_AllColumns_Success() {
		Select select = new Select();
		Assert.assertEquals(select.toString(), "SELECT *");
	}

	@Test()
	void ofString_RawStatement_Success() {
		Assert.assertEquals(Select.ofString("id,name,email").toString(), "SELECT id,name,email");
	}

	private static class SelectData {
		private final String[] columns;
		private final String result;

		SelectData(String[] columns, String result) {
			this.columns = columns;
			this.result = result;
		}

		@Override
		public String toString() {
			return new StringJoiner(", ", SelectData.class.getSimpleName() + "[", "]")
					.add("columns=" + Arrays.toString(columns))
					.add("result='" + result + "'")
					.toString();
		}
	}
}