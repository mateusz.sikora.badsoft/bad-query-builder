package pl.badsoft.bqb.condition;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pl.badsoft.bqb.exception.ConditionValidatorException;

import static java.time.Month.SEPTEMBER;
import static pl.badsoft.bqb.condition.ConditionOperator.EQUAL;
import static pl.badsoft.bqb.condition.ConditionOperator.IN;
import static pl.badsoft.bqb.condition.ConditionOperator.IS_NULL;

public class ConditionTest {

	@Test
	void condition_ConditionsNotValid_Exception() {
		Condition condition = Condition.of("name", ConditionOperator.EQUAL, "Mateusz").and(null);
		Assert.assertThrows(ConditionValidatorException.class, () -> condition.toString());
	}

	@Test(dataProvider = "conditionProvider")
	void condition_FomratValue_Success(String column, ConditionOperator operator, Object value, String result) {
		Condition condition = Condition.of(column, operator, value);
		Assert.assertEquals(condition.toString(), result);
	}

	@DataProvider(name = "conditionProvider")
	Object[][] singleConditionProvider() {
		return new Object[][]{
				{"user_id", IS_NULL, null, "user_id IS NULL"},
				{"user_id", EQUAL, "Mateusz", "user_id ='Mateusz'"},
				{"user_id", EQUAL, 123456, "user_id =123456"},
				{"user_id", EQUAL, true, "user_id =true"},
				{"user_id", EQUAL, LocalDate.of(2023, SEPTEMBER, 1), "user_id ='2023-09-01'"},
				{"user_id", EQUAL, LocalDateTime.of(2023, SEPTEMBER, 1, 12, 15), "user_id ='2023-09-01T12:15:00'"},
				{"user_id", IN, List.of(1, 2, 3, 4), "user_id IN (1,2,3,4)"},
				{"user_id", IN, List.of("a", "b", "c", "d"), "user_id IN ('a','b','c','d')"}
		};
	}
}