package pl.badsoft.bqb.join;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pl.badsoft.bqb.exception.RequiredValueException;

public class JoinTest {

	@Test(dataProvider = "valueDataProvider")
	void join_Section_Success(JoinType joinType, String tableName, String columnLeft, String columnRight, String result) {
		Join join = new Join(joinType, tableName, columnLeft, columnRight);
		Assert.assertEquals(join.toString(), result);
	}

	@DataProvider(name = "valueDataProvider")
	Object[][] valueDataProvider() {
		return new Object[][]{
				{JoinType.JOIN, "person p", "u.person_id", "p.id", "JOIN person p ON u.person_id=p.id"},
				{JoinType.LEFT_JOIN, "person p", "u.person_id", "p.id", "LEFT JOIN person p ON u.person_id=p.id"},
				{JoinType.RIGHT_JOIN, "person p", "u.person_id", "p.id", "RIGHT JOIN person p ON u.person_id=p.id"},
				{JoinType.FULL_JOIN, "person p", "u.person_id", "p.id", "FULL JOIN person p ON u.person_id=p.id"}
		};
	}

	@Test(dataProvider = "wrongDataProvider")
	void join_JoinTypeNull_Excpetion(JoinType joinType, String tableName, String columnLeft, String columnRight) {
		Join join = new Join(joinType, tableName, columnLeft, columnRight);
		Assert.assertThrows(RequiredValueException.class, () -> join.toString());
	}

	@DataProvider(name = "wrongDataProvider")
	Object[][] wrongDataProvider() {
		return new Object[][]{
				{null, "person p", "u.person_id", "p.id"},
				{JoinType.LEFT_JOIN, null, "u.person_id", "p.id"},
				{JoinType.RIGHT_JOIN, "person p", null, "p.id"},
				{JoinType.FULL_JOIN, "person p", "u.person_id", null}
		};
	}
}