package pl.badsoft.bqb;

import org.testng.Assert;
import org.testng.annotations.Test;

public class FromTest {

	@Test
	void from_TableNameWithoutAllias_Success() {
		From from = new From("person");
		Assert.assertEquals(from.toString(), "FROM person");
	}

	@Test
	void of_TableNameWithoutAllias_Success() {
		Assert.assertEquals(From.of("person").toString(), "FROM person");
	}

	@Test
	void from_TableNameWithAllias_Success() {
		From from = new From("person", "p");
		Assert.assertEquals(from.toString(), "FROM person p");
	}

	@Test
	void of_TableNameWithAllias_Success() {
		Assert.assertEquals(From.of("person", "p").toString(), "FROM person p");
	}

	@Test()
	void ofString_RawStatement_Success() {
		Assert.assertEquals(From.ofString("user u").toString(), "FROM user u");
	}

}