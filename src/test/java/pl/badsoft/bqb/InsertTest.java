package pl.badsoft.bqb;

import org.testng.Assert;
import org.testng.annotations.Test;
import pl.badsoft.bqb.exception.RequiredValueException;

public class InsertTest {

	@Test
	void insert_TableNameAndColumns_Success() {
		Insert insert = new Insert("user", "login", "password", "email");
		Assert.assertEquals(insert.toString(), "INSERT INTO user (login,password,email)");
	}

	@Test
	void insert_NoColumns_Exception() {
		Insert insert = new Insert("user");
		Assert.assertThrows(RequiredValueException.class, insert::toString);
	}

	@Test
	void insert_BlankTablename_Exception() {
		Insert insert = new Insert("", "login", "password", "email");
		Assert.assertThrows(RequiredValueException.class, insert::toString);
	}

	@Test
	void of_TableNameAndColumns_Success() {
		Assert.assertEquals(Insert.of("user", "login", "password", "email").toString(), "INSERT INTO user (login,password,email)");
	}
}