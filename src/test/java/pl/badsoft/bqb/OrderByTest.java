package pl.badsoft.bqb;

import java.util.List;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pl.badsoft.bqb.util.Sort;

public class OrderByTest {

	@Test(dataProvider = "dataProvider")
	void where_Condition_Success(List<Sort> columns, String result) {
		Sort[] sorts = columns.toArray(new Sort[0]);
		OrderBy orderBy = new OrderBy(sorts);
		Assert.assertEquals(orderBy.toString(), result);
	}

	@DataProvider(name = "dataProvider")
	Object[][] dataProvider() {
		return new Object[][]{
				{List.of(new Sort("id")), "ORDER BY id ASC"},
				{List.of(new Sort("id", Sort.Direction.DESC)), "ORDER BY id DESC"},
				{List.of(new Sort("name", Sort.Direction.ASC),
						new Sort("lastname", Sort.Direction.DESC)), "ORDER BY name ASC,lastname DESC"}
		};
	}
}