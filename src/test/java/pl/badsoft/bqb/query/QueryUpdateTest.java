package pl.badsoft.bqb.query;

import java.util.List;
import java.util.StringJoiner;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pl.badsoft.bqb.Value;
import pl.badsoft.bqb.Where;
import pl.badsoft.bqb.condition.Condition;
import pl.badsoft.bqb.exception.RequiredValueException;

import static pl.badsoft.bqb.condition.ConditionOperator.EQUAL;

public class QueryUpdateTest {

	@Test
	void queryUpdate_UpdateDateWihtouWhere_Success() {
		QueryUpdate queryUpdate = QueryUpdate.builder()
				.table("person")
				.values(List.of(new Value("status", "ACTIVE")))
				.build();

		Assert.assertEquals(queryUpdate.toString(), "UPDATE person SET status='ACTIVE'");
	}

	@Test(dataProvider = "valueUpdateTestDataProvider")
	void queryUpdate_Statement_Success(QueryUpdateTest.UpdateTestData updateTestData) {
		QueryUpdate queryUpdate = QueryUpdate.builder()
				.table(updateTestData.table)
				.values(updateTestData.values)
				.where(updateTestData.where)
				.build();

		Assert.assertEquals(queryUpdate.toString(), updateTestData.result);
	}

	@DataProvider(name = "valueUpdateTestDataProvider")
	QueryUpdateTest.UpdateTestData[] valueDataProvider() {
		return new QueryUpdateTest.UpdateTestData[]{
				new QueryUpdateTest.UpdateTestData("person",
						List.of(new Value("status", "BLOCKED"), new Value("is_blocked", true)),
						null,
						"UPDATE person SET status='BLOCKED', is_blocked=true"),
				new QueryUpdateTest.UpdateTestData("person", List.of(new Value("name", "Adam"),
						new Value("start_date_time", "2021-08-13T13:07:00")),
						new Where(Condition.of("id", EQUAL, 100)),
						"UPDATE person SET name='Adam', start_date_time='2021-08-13T13:07:00' WHERE id =100")
		};
	}


	@Test(expectedExceptions = RequiredValueException.class)
	public void queryUpdate_TableNameIsNull_Exception() {
		QueryUpdate.builder().table(null).build();
	}

	private static class UpdateTestData {
		private final String table;
		private final List<Value> values;
		private final Where where;
		private final String result;

		UpdateTestData(String table, List<Value> values, Where where, String result) {
			this.table = table;
			this.values = values;
			this.where = where;
			this.result = result;
		}

		@Override
		public String toString() {
			return new StringJoiner(", ", QueryUpdateTest.UpdateTestData.class.getSimpleName() + "[", "]")
					.add("table=" + table)
					.add("values=" + String.join(",", values.stream().map(Value::toString).toString()))
					.add("where=" + where)
					.add("result='" + result + "'")
					.toString();
		}
	}
}