package pl.badsoft.bqb.query;

import org.testng.Assert;
import org.testng.annotations.Test;
import pl.badsoft.bqb.From;
import pl.badsoft.bqb.OrderBy;
import pl.badsoft.bqb.Select;
import pl.badsoft.bqb.Where;
import pl.badsoft.bqb.condition.Condition;
import pl.badsoft.bqb.condition.ConditionOperator;
import pl.badsoft.bqb.join.Join;
import pl.badsoft.bqb.join.JoinType;
import pl.badsoft.bqb.util.PageParams;
import pl.badsoft.bqb.util.Sort;

public class QuerySelectTest {

	@Test
	void querySelect_ThreeColumns_Success() {
		QuerySelect querySelect = QuerySelect.builder()
				.select(new Select("id", "name", "last_name"))
				.from(new From("person"))
				.build();

		Assert.assertEquals(querySelect.toString(), "SELECT id,name,last_name FROM person");
	}

	@Test
	void querySelect_ThreeColumnsWithWhereClause_Success() {
		QuerySelect querySelect = QuerySelect.builder()
				.select(new Select("id", "name", "last_name"))
				.from(new From("person"))
				.where(new Where(Condition.of("name", ConditionOperator.EQUAL, "Mateusz")))
				.build();

		Assert.assertEquals(querySelect.toString(), "SELECT id,name,last_name FROM person WHERE name ='Mateusz'");
	}

	@Test
	void querySelect_TwoConditionsInWhereClause_Success() {
		QuerySelect querySelect = QuerySelect.builder()
				.select(new Select("id", "name", "last_name"))
				.from(new From("person"))
				.where(new Where(Condition.of("name", ConditionOperator.EQUAL, "Mateusz")
						.or(Condition.of("age", ConditionOperator.NOT_EQUAL, 20))))
				.build();

		Assert.assertEquals(querySelect.toString(), "SELECT id,name,last_name FROM person WHERE name ='Mateusz'" +
				" OR age !=20");
	}

	@Test
	void querySelect_WithJoin_Success() {
		QuerySelect querySelect = QuerySelect.builder()
				.select(new Select())
				.from(new From("user", "u"))
				.join(new Join(JoinType.LEFT_JOIN, "person p", "u.person_id", "p.id"))
				.build();

		Assert.assertEquals(querySelect.toString(), "SELECT * FROM user u LEFT JOIN person p ON u.person_id=p.id");
	}

	@Test
	void querySelect_WithJoinAndWhere_Success() {
		QuerySelect querySelect = QuerySelect.builder()
				.select(new Select("u.email", "p.name"))
				.from(new From("user", "u"))
				.join(new Join(JoinType.LEFT_JOIN, "person p", "u.person_id", "p.id"))
				.where(new Where(Condition.of("p.phone", ConditionOperator.IS_NOT_NULL)))
				.build();

		Assert.assertEquals(querySelect.toString(), "SELECT u.email,p.name FROM user u " +
				"LEFT JOIN person p ON u.person_id=p.id " +
				"WHERE p.phone IS NOT NULL");
	}

	@Test
	void querySelect_SimpleBuilderWIthWildcard_Success() {
		QuerySelect querySelect = QuerySelect.builder()
				.select()
				.from("user")
				.build();

		Assert.assertEquals(querySelect.toString(), "SELECT * FROM user");
	}

	@Test
	void querySelect_SelectCount_Success() {
		QuerySelect querySelect = QuerySelect.builder()
				.select(new Select("COUNT(*)"))
				.from("user")
				.build();

		Assert.assertEquals(querySelect.toString(), "SELECT COUNT(*) FROM user");
	}

	@Test
	void querySelect_SelectWithPagination_Success() {
		QuerySelect querySelect = QuerySelect.builder()
				.select(new Select("*"))
				.from("user")
				.pageParams(PageParams.of(500, 2000))
				.build();

		Assert.assertEquals(querySelect.toString(), "SELECT * FROM user LIMIT 500 OFFSET 2000");
	}


	@Test
	void querySelect_SelectWithOrderBy_Success() {
		QuerySelect querySelect = QuerySelect.builder()
				.select(new Select("*"))
				.from("user")
				.orderBy(new OrderBy(Sort.of("id", Sort.Direction.ASC)))
				.build();

		Assert.assertEquals(querySelect.toString(), "SELECT * FROM user ORDER BY id ASC");
	}

	@Test
	void querySelect_SelectWithOrderByTwoColumns_Success() {
		QuerySelect querySelect = QuerySelect.builder()
				.select(new Select("*"))
				.from("user")
				.orderBy(new OrderBy(Sort.of("name", Sort.Direction.ASC),
						Sort.of("lastname", Sort.Direction.DESC)))
				.build();

		Assert.assertEquals(querySelect.toString(), "SELECT * FROM user ORDER BY name ASC,lastname DESC");
	}

	@Test
	void querySelect_SelectWithOrderByAndPage_Success() {
		QuerySelect querySelect = QuerySelect.builder()
				.select(new Select("*"))
				.from("user")
				.orderBy(new OrderBy(Sort.of("name", Sort.Direction.ASC),
						Sort.of("lastname", Sort.Direction.DESC)))
				.pageParams(PageParams.of(500, 0))
				.build();

		Assert.assertEquals(querySelect.toString(), "SELECT * FROM user ORDER BY name ASC,lastname DESC LIMIT 500 OFFSET 0");
	}

	@Test
	void querySelect_SelectWithOrderByAndPage_SuccessOveridePageOrder() {
		QuerySelect querySelect = QuerySelect.builder()
				.select(new Select("*"))
				.from("user")
				.orderBy(new OrderBy(Sort.of("name", Sort.Direction.ASC)))
				.pageParams(PageParams.of(500, 0, "id"))
				.build();

		Assert.assertEquals(querySelect.toString(), "SELECT * FROM user ORDER BY name ASC LIMIT 500 OFFSET 0");
	}

	@Test
	void querySelect_RawStatement_Success() {
		QuerySelect querySelect = QuerySelect.builder()
				.select(Select.ofString("u.email, u.name"))
				.from(From.ofString("user u"))
				.where(Where.ofString("u.phone IS NOT NULL"))
				.build();

		Assert.assertEquals(querySelect.toString(), "SELECT u.email, u.name FROM user u WHERE u.phone IS NOT NULL");
	}

}