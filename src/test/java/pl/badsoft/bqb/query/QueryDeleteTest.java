package pl.badsoft.bqb.query;

import org.testng.Assert;
import org.testng.annotations.Test;
import pl.badsoft.bqb.Where;
import pl.badsoft.bqb.condition.Condition;
import pl.badsoft.bqb.exception.RequiredValueException;

import static pl.badsoft.bqb.condition.ConditionOperator.EQUAL;

public class QueryDeleteTest {

	@Test
	void queryDelete_DeleteAllData_Success() {
		QueryDelete queryDelete = QueryDelete.builder()
				.table("person")
				.build();

		Assert.assertEquals(queryDelete.toString(), "DELETE FROM person");
	}

	@Test
	void queryDelete_DeleteDataWithWhereClause_Success() {
		QueryDelete queryDelete = QueryDelete.builder()
				.table("person")
				.where(new Where(Condition.of("name", EQUAL, "Mateusz")))
				.build();

		Assert.assertEquals(queryDelete.toString(), "DELETE FROM person WHERE name ='Mateusz'");
	}

	@Test(expectedExceptions = RequiredValueException.class)
	public void delete_TableNameAndWhereIsNull_Exception() {
		QueryDelete.builder().table(null).where(null).build();
	}
}