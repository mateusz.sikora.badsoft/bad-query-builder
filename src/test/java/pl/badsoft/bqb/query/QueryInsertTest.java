package pl.badsoft.bqb.query;

import java.util.Collections;
import java.util.List;
import java.util.StringJoiner;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pl.badsoft.bqb.Value;
import pl.badsoft.bqb.exception.RequiredValueException;

public class QueryInsertTest {


	@Test(dataProvider = "valueDataProvider")
	void insert_Statement_Success(QueryInsertTest.InsertData insertData) {
		QueryInsert queryInsert = QueryInsert.builder()
				.table(insertData.table)
				.values(insertData.values)
				.build();

		Assert.assertEquals(queryInsert.toString(), insertData.result);
	}

	@DataProvider(name = "valueDataProvider")
	QueryInsertTest.InsertData[] valueDataProvider() {
		return new QueryInsertTest.InsertData[]{
				new QueryInsertTest.InsertData("person", List.of(new Value("id", 1), new Value("name", "John"),
						new Value("phone", 222333444)),
						"INSERT INTO person (id,name,phone) VALUES (1,'John',222333444)"),
				new QueryInsertTest.InsertData("person", List.of(new Value("id", 2), new Value("name", "Adam"),
						new Value("login_date_time", "2021-08-13T13:07:00")),
						"INSERT INTO person (id,name,login_date_time) VALUES (2,'Adam','2021-08-13T13:07:00')"),
				new QueryInsertTest.InsertData("person", List.of(new Value("id", 3), new Value("name", "Peter"),
						new Value("is_active", null)),
						"INSERT INTO person (id,name,is_active) VALUES (3,'Peter',null)")
		};
	}

	@Test(expectedExceptions = RequiredValueException.class)
	public void insert_TableNameIsNull_Exception() {
		QueryInsert.builder().table(null).build();
	}

	@Test(expectedExceptions = RequiredValueException.class)
	public void insert_ValuesIsEmpty_Exception() {
		QueryInsert.builder().values(Collections.emptyList()).build();
	}

	private static class InsertData {
		private final String table;
		private final List<Value> values;
		private final String result;

		InsertData(String table, List<Value> values, String result) {
			this.table = table;
			this.values = values;
			this.result = result;
		}

		@Override
		public String toString() {
			return new StringJoiner(", ", QueryInsertTest.InsertData.class.getSimpleName() + "[", "]")
					.add("table=" + table)
					.add("values=" + String.join(",", values.stream().map(Value::toString).toString()))
					.add("result='" + result + "'")
					.toString();
		}
	}

}