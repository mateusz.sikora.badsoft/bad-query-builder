package pl.badsoft.bqb;

import java.time.LocalDate;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pl.badsoft.bqb.condition.Condition;
import pl.badsoft.bqb.condition.ConditionOperator;

import static java.time.Month.JANUARY;
import static pl.badsoft.bqb.condition.ConditionOperator.AND;
import static pl.badsoft.bqb.condition.ConditionOperator.EQUAL;
import static pl.badsoft.bqb.condition.ConditionOperator.GREATER;
import static pl.badsoft.bqb.condition.ConditionOperator.GREATER_OR_EQUALS;
import static pl.badsoft.bqb.condition.ConditionOperator.IN;
import static pl.badsoft.bqb.condition.ConditionOperator.IS_NOT_NULL;
import static pl.badsoft.bqb.condition.ConditionOperator.LESS;
import static pl.badsoft.bqb.condition.ConditionOperator.LESS_OR_EQUALS;
import static pl.badsoft.bqb.condition.ConditionOperator.OR;

@Slf4j
public class WhereTest {

	@Test(dataProvider = "singleConditionProvider")
	void where_Condition_Success(String column, ConditionOperator operator, Object value, String result) {
		Where where = new Where(Condition.of(column, operator, value));
		Assert.assertEquals(where.toString(), result);
	}

	@DataProvider(name = "singleConditionProvider")
	Object[][] singleConditionProvider() {
		return new Object[][]{
				{"name", EQUAL, "Mateusz", "WHERE name ='Mateusz'"},
				{"money", GREATER_OR_EQUALS, 1000, "WHERE money >=1000"},
				{"my_date", LESS, LocalDate.of(2023, JANUARY, 1), "WHERE my_date <'2023-01-01'"}
		};
	}

	@Test
	void where_ThreeConditions_Success() {
		Condition condition = Condition.of("name", EQUAL, "Mateusz")
				.and(Condition.of("lastname", EQUAL, "Sikora")
						.or(Condition.of("phone", IS_NOT_NULL)));
		Where where = new Where(condition);
		Assert.assertEquals(where.toString(), "WHERE name ='Mateusz' AND lastname ='Sikora' OR phone IS NOT NULL");
	}

	@Test
	void where_MultiConditions_Success() {
		Condition condition = Condition.of("name", EQUAL, "Mateusz")
				.and(Condition.of("age", GREATER, 20)
						.and(Condition.of("phone", IS_NOT_NULL)
								.and(Condition.of("position", IN, List.of(1, 2, 3)))));
		Where where = new Where(condition);
		Assert.assertEquals(where.toString(), "WHERE name ='Mateusz' AND age >20 AND phone IS NOT NULL AND position IN (1,2,3)");
	}

	@Test
	void where_FourConditionsWithTwoSectionByAND_Success() {
		Condition conditionName = Condition.of("name", EQUAL, "Mateusz").and(Condition.of("lastname", EQUAL, "Sikora"));
		Condition conditionAge = Condition.of("age", GREATER_OR_EQUALS, 18).and(Condition.of("age", LESS_OR_EQUALS, 50));
		Where where = new Where(AND, conditionName, conditionAge);
		Assert.assertEquals(where.toString(), "WHERE (name ='Mateusz' AND lastname ='Sikora') AND (age >=18 AND age <=50)");
	}

	@Test
	void where_FourConditionsWithTwoSectionByOR_Success() {
		Condition conditionName = Condition.of("name", EQUAL, "Mateusz").and(Condition.of("lastname", EQUAL, "Sikora"));
		Condition conditionAge = Condition.of("age", GREATER_OR_EQUALS, 18).and(Condition.of("age", LESS_OR_EQUALS, 50));
		Where where = new Where(OR, conditionName, conditionAge);
		Assert.assertEquals(where.toString(), "WHERE (name ='Mateusz' AND lastname ='Sikora') OR (age >=18 AND age <=50)");
	}

	@Test()
	void ofString_RawStatement_Success() {
		Assert.assertEquals(Where.ofString("email = 'test@test.pl' AND id > 10").toString(), "WHERE email = 'test@test.pl' AND id > 10");
	}

	@Test
	void of_ThreeConditions_Success() {
		Condition condition = Condition.of("name", EQUAL, "Mateusz")
				.and(Condition.of("lastname", EQUAL, "Sikora")
						.or(Condition.of("phone", IS_NOT_NULL)));
		Assert.assertEquals(Where.of(condition).toString(), "WHERE name ='Mateusz' AND lastname ='Sikora' OR phone IS NOT NULL");
	}
}